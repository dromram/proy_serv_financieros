**CONSOLIDACION Y VALIDACION EN EL PROCESO DEL PILA**

Es un proceso que se realiza mes a mes en el área de ARL donde verifican los pagos realizados por empleados e independientes afiliados al plan de riesgo laborales sura.

La optimización nace de la necesidad presentada por la auxiliar de la ARL, ya que dicho proceso a cierre de mes tarda demasiado tiempo operativo, por lo cual desde el área de información estratégica y analítica financiera,  se desarrollo un codigo en R-Studio el cual optimiza el tiempo de ejecución y automatiza la operatividad, cambiando de 1 semana de operación a 1.5 horas en ejecución.


**REQUERIMIENTOS:**
**1. Excel**
**2. Archivos .Zips**
**3. R-studio**
---

## Funcionamiento del código en R

El script se diseñó en R-studio, y tiene como objetivo optimizar el tiempo de operatividad del usuario, en cuanto a la estrucuracion de las bases de datos, validaciones de información, entre otras. 

**Paso a paso de la ejecución**

1. Se descargan las librerias: ** readr, dplyr, xlsx y data.table** para el funcionamento del código
2. Ubica todos los archivos terminados en formato .Zip y descomprime cada uno de ellos.
3. Se crea una funcion para organizar todos los archivos **.txt** en un mismo formato
4. Automáticamente estructura la base consolidada con toda la información de las empresas, con las columnas y nombres requeridas. 
5. Posteriormente cada llamado de la función lo guarda en una lista y realiza el apilamiento de los archivos **.txt** para que finalmente quede en un solo archivo toda la información requerida.
6. Se estructura la base final, se descarga y queda lista para su validación
7. Finalmente se importa la base de datos llamada ARUS, la cual contiene la información para validar con la base final lalmada ARL_PILA
8. Luego de importarla se estructura la base de tal manera que quede eficaz para el cruce final. Creando una columnas con condicionales propuestos por el usuario.
9. Se realiza el cruce final y se valida con el usuario.
10. Se exportan los archivos para la entrega final.

---
